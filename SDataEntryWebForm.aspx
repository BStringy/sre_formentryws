﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SDataEntryWebForm.aspx.cs" Inherits="SRE_OnlineFormEntryWS.SDataEntryWebForm" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Data Entry Form</title>
</head>

<body>
    <form id="form1" runat="server">
         <h1>
            <asp:label ID="Title" runat="server" Text="Form Entry Service" style="font-size:30px"></asp:label><br />
            enter data into the form below.
        </h1>
        <div>
             <p style="margin-top:2.5em"></p>
            <asp:Label ID="LabelFName" runat="server" Text="First Name"></asp:Label><br /><asp:TextBox ID="TextBoxFName" runat="server" Text=""  style="height: 26px" autocomplete="off" /> <br /> <!-- utilsing the auto complete off fucntion 
                we can prevent secure details being given away by autocomplete -->
        
             <p style="margin-top:1em"></p>
            <asp:Label ID="LabelSName" runat="server" Text="Surname"></asp:Label><br /><asp:TextBox ID="TextBoxSName" runat="server" Text=""  style="height: 26px" autocomplete="off"/> <br />
        
             <p style="margin-top:1em"></p>
            <asp:Label ID="LabelAge" runat="server" Text="Age"></asp:Label><br /><asp:TextBox ID="TextBoxAge" runat="server" Text=""  style="height: 26px" autocomplete="off"/> <br />
      
            <p style="margin-top:1em"></p>
           <asp:Label ID="LabelGender" runat="server" Text="Gender"></asp:Label><br /> <asp:DropDownList ID="DDLGender" runat="server"> 
                <asp:ListItem Enabled="true" Text="Select Gender" Value="-1"></asp:ListItem>
                <asp:ListItem Text="Male" Value="1"></asp:ListItem>
                <asp:ListItem Text="Female" Value="2"></asp:ListItem>
            </asp:DropDownList><br />

            <p style="margin-top:1em"></p>
            <asp:Label ID="LabelJob" runat="server" Text="Job"></asp:Label><br /> <asp:DropDownList ID="DDLJob" runat="server">
                <asp:ListItem Enabled="true" Text="Select Job" Value="-1"></asp:ListItem>
                <asp:ListItem Text="Scientist" Value="1"></asp:ListItem>
                <asp:ListItem Text="Engineer" Value="2"></asp:ListItem>
                <asp:ListItem Text="Janitor" Value="3"></asp:ListItem>
                <asp:ListItem Text="Team Lead" Value="4"></asp:ListItem>
            </asp:DropDownList><br />
            <p style="margin-top: 2.5em"></p>
            <asp:Label ID="labelPass" runat="server" Text="Please Enter Password"></asp:Label><p style="margin-left:1em"></p><asp:TextBox ID="TextBoxPass" runat="server" Text="" style="height: 26px" AutoComplete="off " TextMode="Password" /><br /> <!-- as we could not implement a more foramal form of authetication without overcomplecating the product, we implemnted a password for data entry to prevent anyone without the password from entering data-->
            <p style="margin-top: 2.5em"></p>
            <asp:Button ID="Button1" runat="server" Text="Confirm" OnClick="Button1_Click" style="height: 26px" />
            <p style="margin-top: 5em"></p>
            <asp:Label ID="labelRead1" runat="server" Text=""></asp:Label><br />
            <asp:Label ID="labelRead2" runat="server" Text=""></asp:Label><br />
            <asp:Label ID="labelRead3" runat="server" Text=""></asp:Label><br />
            <asp:Label ID="labelRead4" runat="server" Text=""></asp:Label><br />
            <asp:Label ID="labelRead5" runat="server" Text=""></asp:Label><br />
            <asp:Label ID="labelRead6" runat="server" Text=""></asp:Label><br />
        </div>
    </form>
</body>
</html>
