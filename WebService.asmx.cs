﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace SRE_OnlineFormEntryWS
{
    /// <summary>
    /// Summary description for WebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    

    public class WebService : System.Web.Services.WebService
    {
        [WebMethod]
        public int UID(List<int> listID)
        {
            int ID = 0;
            for (int i = 0; i < listID.Count; i++)
            {
                ID = ID + listID[i];
            }
            return ID;
        }
    }
}
