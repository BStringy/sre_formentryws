﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace SRE_OnlineFormEntryWS
{
   
    public partial class SDataEntryWebForm : System.Web.UI.Page
    {
        

        protected void Page_Load(object sender, EventArgs e)
        {
            

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
           

            // If statment should act as a very basic form of vaidation in which people without the password will be able to access the data or create false data
            if (TextBoxPass.Text.Length > 0 && TextBoxPass.Text != "Password")
            {
                labelRead1.Text = "Please enter correct password";
                TextBoxPass.Text = "";
                return;

            }
            else if (TextBoxPass.Text.Length == 0)
            {
                labelRead1.Text = "Please Enter Password";
                TextBoxPass.Text = "";
                return;

            }
            else if (TextBoxPass.Text == "Password")
            {
                WebService webService = new WebService();
                Random rnd = new Random();
                int r1 = rnd.Next(0, 9);
                int r2 = rnd.Next(0, 9);
                int r3 = rnd.Next(0, 9);
                List<int> listID = new List<int> { r1, r2, r3 };
                String fname;
                String sname;
                String age;
                String Gender;
                String Job;
                String FnameOut = "";
                String SnameOut = "";
                String ageout = "";
                String GenderOut = "";
                String JobOut = "";
                labelRead1.Text = "";
                labelRead2.Text = " ";
                labelRead3.Text = " ";
                labelRead4.Text = " ";
                labelRead5.Text = " ";
                labelRead6.Text = " ";
                fname = TextBoxFName.Text;
                sname = TextBoxSName.Text;
                age = TextBoxAge.Text;
                Gender = DDLGender.Text;
                Job = DDLJob.Text;
                // if statement below shows example of input validation, originally implemented to prevent invalid data entry can double as a prevention for dangerous data entry. 
                if (fname.Any(char.IsLetter))
                {
                    FnameOut = fname;
                }
                else if (fname.Any(char.IsDigit) || fname.Any(char.IsSymbol))
                {
                    labelRead1.Text = "Invalid Parameters in the Firstname field";
                    return;
                }
                else if (fname == String.Empty)
                {
                    labelRead1.Text = "Firstname Field Empty!";
                    return;
                }

                if (sname.Any(char.IsLetter))
                {
                    SnameOut = sname;

                }
                else if (sname.Any(char.IsDigit) || sname.Any(char.IsSymbol))
                {
                    labelRead1.Text = "Invalid parameters in the Surname field";
                    return;
                }
                else if (sname == String.Empty)
                {
                    labelRead1.Text = "Surname Field Empty!";
                    return;
                }
                if (age.Any(char.IsDigit))
                {
                    ageout = age;

                }
                else if (age.Any(char.IsLetter) || age.Any(char.IsSymbol))
                {
                    labelRead1.Text = "Invalid Parameters in the Age field";
                    return;
                }
                else if (age == String.Empty)
                {
                    labelRead1.Text = "Age Field Empty!";
                    return;
                }
                if (Gender == "-1")
                {
                    labelRead1.Text = "Please select a Gender";
                    return;
                }
                else if (Gender == "1")
                {
                    GenderOut = "Male";

                }
                else if (Gender == "2")
                {
                    GenderOut = "Female";
                }

                if (Job == "-1")
                {
                    labelRead1.Text = "Please Select a Relevant Job Title";
                    return;
                }
                else if (Job == "1")
                {
                    JobOut = "Scientist";
                }
                else if (Job == "2")
                {
                    JobOut = "Engineer";
                }
                else if (Job == "3")
                {
                    JobOut = "Janitor";

                }
                else if (Job == "4")
                {
                    JobOut = "Team Lead";
                }

                labelRead1.Text = "Data Confirmed ";
                labelRead2.Text = "Your ID is: MS-" + webService.UID(listID);
                labelRead3.Text = "Full Name: " + FnameOut + " " + SnameOut;
                labelRead4.Text = "Age: " + ageout;
                labelRead5.Text = "Gender: " + GenderOut;
                labelRead6.Text = "Job: " + JobOut;
                TextBoxPass.Text = "";
                TextBoxFName.Text = "";
                TextBoxSName.Text = "";
                TextBoxAge.Text = "";
                DDLGender.Text = "-1";
                DDLJob.Text = "-1";
            }
        }
    }
}